namespace :debug do
    desc "Generates and modifies debug data"
    task generate_data: :environment do
        Instance.find_by_instance_name('debug').destroy if Instance.exists?(instance_name: 'debug')
        instance = FactoryGirl.create(:instance, instance_name: 'debug')
        instance.company = FactoryGirl.create(:company)

        category = instance.item_categories.create(name: 'Collectables')
        ['Trading Cards', 'Stamps', 'Dishware'].each do |name|
            category.item_sub_categories.create(name: name)
        end

        category = instance.item_categories.create(name: 'Antiques')
        ['Furniture', 'Other'].each do |name| 
            category.item_sub_categories.create(name:name)
        end

        category = instance.item_categories.create(name: 'Electronics')
        ['Computers', 'Audio', 'Games'].each do |name| 
            category.item_sub_categories.create(name:name)
        end

        category = instance.item_categories.create(name: 'Jewerly')
        ['Rings', 'Necklaces', 'Watches'].each do |name| 
            category.item_sub_categories.create(name:name)
        end

        category = instance.item_categories.create(name: 'Home & Garden')
        ['Garden', 'Home Decore', 'Furniture', 'Outdoors'].each do |name| 
            category.item_sub_categories.create(name:name)
        end

        category = instance.item_categories.create(name: 'Sporting Goods');
        ['Sports Equipment', 'Workout Equipment'].each do |name| 
            category.item_sub_categories.create(name:name)
        end
        FactoryGirl.create(:user, instance_id: instance.id)

        3.times do 
            item = instance.items.new(title: 'XBox One', featured: true, price: 129.99)
            item.description = "Introducing Xbox One. Where the best games, multiplayer, and your favorite movies, music, sports, and live TV come together in one place. Xbox One games look and feel incredibly real, with cinematic gameplay that rivals Hollywood. Watch TV or chat with friends on Skype while you play, and keep on playing while smarter matchmaking happens behind the scenes. With Xbox One, you can snap two things side-by-side on your TV, and switch from one to another instantly. Cloud-powered and built for the digital age, Xbox One is designed to keep getting better over time."
            item.item_category = ItemCategory.find_by_name('Electronics')
            item.item_sub_category = ItemCategory.find_by_name('Electronics').item_sub_categories.find_by_name('Games')
            if item.save
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'xbox.jpg'))
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'xbox2.jpg'))
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'xbox3.jpg'))
            end

            item = instance.items.new(title: 'Wii U', price: 100)
            item.item_category = ItemCategory.find_by_name('Electronics')
            item.item_sub_category = ItemCategory.find_by_name('Electronics').item_sub_categories.find_by_name('Games')
            item.description = "Experience revolutionary, new ways to play together with the innovative Wii U GamePad controller. Play in up to full 1080p HD for the first-time ever on a Nintendo video game system Backwards compatible:almost all of your existing Wii games and accessories will work. Includes: Black 32GB Wii U Console, New Super Mario Bros U and New Super Luigi U Games on one disc, Gamepad, Gamepad Cradle, GamePad Stand support, & Console Stand"
            if item.save
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'wiiu.jpg'))
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'wiiu1.jpg'))
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'wiiu2.jpg'))
            end

            item = instance.items.new(title: 'Xbox One Ryse', price: 60)
            item.description = "Amidst the chaos of the late Roman Empire, become soldier Marius Titus and embark on a perilous campaign to avenge the death of your family and defend the honor of Rome. In Gladiator Mode, step into the Colosseum and fight for the glory, spectacle, and entertainment of the crowds. The downloadable version of this game."
            item.item_category = ItemCategory.find_by_name('Electronics')
            item.item_sub_category = ItemCategory.find_by_name('Electronics').item_sub_categories.find_by_name('Games')
            if item.save
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'ryse.jpg'))
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'ryse2.jpg'))
            end


            item = instance.items.new(title: 'Xbox One Controller', price: 40) 
            item.description = "Experience the action like never before with the Xbox One Wireless Controller. New Impulse Triggers deliver fingertip vibration feedback, so you can feel every jolt and crash in high definition.* Redesigned thumbsticks and an all new D-pad provide greater precision. And the entire controller fits more comfortably in your hands. There's also a new expansion port with high-speed data transfer for clearer chat audio when using a compatible headset.** With over 40 innovations, it’s simply the best controller Xbox has ever made. Includes two AA batteries."
            item.item_category = ItemCategory.find_by_name('Electronics')
            item.item_sub_category = ItemCategory.find_by_name('Electronics').item_sub_categories.find_by_name('Games')
            if item.save
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'xbox-controller.jpg'))
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'xbox-controller2.jpg'))
            end

            item = instance.items.new(title: 'Xbox One - Dead Rising 3', price: 40)
            item.description = "Capcom's other zombie franchise debuts on Xbox One with a new open-world adventure set ten years after the events of Dead Rising 2. Players take control of the mechanic Nick Ramos as he battles his way through hordes of zombies in an attempt to escape the fictional city of Los Perdidos before an impending military strike decimates the area. As always, gamers collect and combine various everyday objects they find into brutal zombie-killing weapons, but Dead Rising 3 also places a greater emphasis on customizable vehicles to help Ramos get around the city."
            item.item_category = ItemCategory.find_by_name('Electronics')
            item.item_sub_category = ItemCategory.find_by_name('Electronics').item_sub_categories.find_by_name('Games')
            if item.save
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'xboxrising.jpg'))
                item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'xboxrising.jpg'))
            end
        end
    end
end
