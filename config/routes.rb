Rails.application.routes.draw do

    namespace :browser, path: '/', constraints: lambda {|request| request.subdomain.present? && request.subdomain != 'www'} do
        get '/' => 'item_browser#land', :as => :landing
        get '/search' => 'item_browser#search', :as => :search
        get '/item/:id' => 'item_browser#show_item', :as => :item
        get '/category/:id' => 'item_browser#browse_category', :as => :category
    end

    namespace :homepage, path: '/' do
        get '/' => 'landing#land', :as => :landing
        get 'signup' => 'signup#signup', :as => :signup
        post 'signup' => 'signup#do_signup'
        get 'billing' => 'signup#billing', :as => :billing
        post 'billing' => 'signup#do_billing'
        get 'signin' => 'signin#show', :as => :signin
        post 'signin' => 'signin#signin'
        get 'signout' => 'signin#signout', :as => :signout
        get 'reset' => 'signin#reset_form', :as => :reset_password_form
        post 'reset' => 'signin#send_reset_email'
        get 'reset_password/:token' => 'signin#reset_password', :as => :reset_password
        post'reset_password/:token' => 'signin#update_password'
    end

    namespace :inventory do 
        get '/account' => 'account#show', :as => :account
        patch '/account' => 'account#update_user'
        get '/' => 'landing#land', :as => :landing
        resources :items do 
            resources :item_photos do
                member do 
                    get 'primary' => 'item_photos#set_primary', :as => :set_primary
                end
            end
        end
        resources :item_categories do 
            resources :item_sub_categories
        end
        scope 'settings' do 
            get 'application' => 'settings#application_settings', :as => :application_settings
            post 'application' => 'settings#update_application_settings'
            get 'apperance' => 'settings#apperance_settings', :as => :apperance_settings
            patch 'apperance' => 'settings#update_apperance_settings'
            get 'remove_logo' => 'settings#remove_logo', :as => :remove_logo 

            scope :billing do 
                get '/' => 'billing#view', :as => :billing
                get 'add_card' => 'billing#new_card', :as => :new_card
                post 'add_card' => 'billing#create_card'
                get 'primary_card' => 'billing#make_primary', :as => :make_primary_card
                get 'delete_card' => 'billing#delete_card', :as => :delete_card
                get 'cancel' => 'billing#confirm_cancel_account', :as => :cancel_account
                post 'cancel' => 'billing#cancel_account'
            end
        end
    end

    namespace :help do 
        get '/' => 'articles#landing', :as => :landing
        get ':article' => 'articles#article', :as => :article
    end
end
