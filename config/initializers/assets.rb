
    Rails.configuration.assets.precompile += %w(inventory.css homepage.css)
    # themes
    Rails.configuration.assets.precompile += %w(browser/themes/hydrogen/black.css 
                                                browser/themes/hydrogen/nova.css
                                                browser/themes/hydrogen/cinder.css
                                                browser/themes/nitrogen/aero.css
                                                browser/themes/nitrogen/burnt.css
                                                browser/themes/nitrogen/red.css
                                               )
    Rails.configuration.assets.precompile += %w(*.woff *.eot *.svg *.tff)
