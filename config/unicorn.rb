working_directory '/mnt/hockd/current'
pid '/mnt/hockd/current/tmp/pids/unicorn.pid'
stderr_path '/mnt/hockd/current/log/unicorn.log'
stdout_path '/mnt/hockd/current/log/unicorn.log'

listen '/tmp/unicorn.hockd.sock'

worker_processes 2

timeout 30

