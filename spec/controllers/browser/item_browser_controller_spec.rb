require 'rails_helper'

RSpec.describe Browser::ItemBrowserController, :type => :controller do

    let(:instance) {create(:instance)}
    let(:item_category) {create(:item_category, instance_id: instance.id)}
    let(:item_sub_category) {create(:item_sub_category, item_category_id: item_category.id)}
    let(:item) {create(:item, instance_id: instance.id, item_category_id: item_category.id,  item_sub_category_id: item_sub_category.id)}

    before(:each) do 
        request.host = "#{instance.instance_name}.example.com"
        allow(controller).to receive(:current_instance).and_return(instance)
    end
    after(:each) do
        expect(response).to render_template("layouts/browser/#{instance.apperance_setting.theme_name}")
    end

    describe "#land" do 
        before(:each) do 
            create_list(:item, 100, instance_id: instance.id, item_category_id: item_category.id, item_sub_category_id: item_sub_category.id)
        end
        context "without filters" do 
            it "should display the first 25 items" do 
                get :land
                expect(assigns(:items).count).to eq(20)
            end
        end 
        context "with page size of 50" do 
            it "should display 50 items" do 
                get :land, per: 50
                expect(assigns(:items).count).to eq(50)
            end
        end
    end
    describe "#show_item" do 
        it "should assign @item" do 
            get :show_item, id: item.id
            expect(assigns(:item)).to eq(item)
        end
    end

    describe "#browse_category" do 
        it "should assign @category" do 
            get :browse_category, id: item_category.id
            expect(assigns(:category)).to eq(item_category)
        end
    end
    describe "#search" do 
        it "should render show_items" do
            get :search
            expect(response).to render_template('browser/item_browser/show_items')
        end
        it "should assign @items" do 
            item
            get :search
            expect(assigns(:items)).to eq([item])
        end
        it "assigns @title" do 
            get :search, q: {title_cont: 'tesst'}
            expect(assigns(:title)).to eq('Searching for "tesst"')
        end
        it "should render layout for theme name" do 
            get :search
        end
    end
end
