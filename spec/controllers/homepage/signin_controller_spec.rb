require 'rails_helper'

RSpec.describe Homepage::SigninController, :type => :controller do

    let(:user) { create(:user, password: 'test12345', password_confirmation: 'test12345') }


    
    describe "#show" do
        it "should respond successful" do
            get :show
            expect(response).to be_successful
        end

    end

    describe "#signin" do 

        describe "with valid credentials" do 
            it "should redirect to account_show_path" do  
                user
                post :signin, email: user.email.address, password: 'test12345'
                expect(response).to redirect_to inventory_landing_path
            end

            it "should set user_id in session" do 
                post :signin, email: user.email.address, password: 'test12345'
                expect(session[:user_id]).to eq(user.id)
            end
        end

        describe "with invalid credentials" do 
            it "should render show" do
                post :signin, email: user.email.address, password: 'nope'
                expect(response).to render_template('show')
            end
            it "should assign @error" do 
                post :signin, email: user.email.address, password: 'nope'
                expect(assigns(:error)).to be_present
            end
        end
    end

    describe "#reset_form" do 
        it "should be successful" do 
            get :reset_form
            expect(response).to be_successful
        end
    end

    describe "#send_reset_email" do 
        context "with valid email" do 
            it "should send email" do 
                expect { 
                    post :send_reset_email, email: user.email.address
                }.to change(ActionMailer::Base.deliveries, :count).by(1)
            end
            it "should update user#password_recovery_token" do
                post :send_reset_email, email: user.email.address
                user.reload
                expect(user.password_recovery_token).to be_present
            end
            it "should render reset_email_sent" do 
                post :send_reset_email, email: user.email.address
                expect(response).to render_template('reset_email_sent')
            end
        end
        context "with invalid email" do 
            it "should render reset form" do 
                post :send_reset_email, email: 'noone@nowhere.com' 
                expect(response).to render_template('reset_form')
            end
            it "should assign @error" do 
                post :send_reset_email, email: 'noone@nowehre.com'
                expect(assigns(:error)).to be_present
            end
        end
    end

    describe "#reset_password"  do
        context "with valid token" do 
            before(:each) do 
                user.update(password_recovery_token: 'aaaa')
            end
            it "should assign @user" do 
                get :reset_password, token: user.password_recovery_token
                expect(assigns(:user)).to eq(user)
            end
            it "render reset_password" do
                get :reset_password, token: user.password_recovery_token
                expect(response).to render_template('reset_password')
            end
        end
        context "with invalid token" do 
            it "should assign @error" do 
                get :reset_password, token: 'xxxxx'
                expect(assigns(:error)).to be_present
            end
            it "should render reset_form" do 
                get :reset_password, token: 'xxxxx'
                expect(response).to render_template('reset_form')
            end
        end
    end 

    describe "#update_password" do
        before(:each) do
            user.update(password_recovery_token: 'abcdef')
        end
        context "with valid password" do
            it "should update password" do
                user
                post :update_password, {
                    token: user.password_recovery_token,
                    password: 'a new password',
                    password_confirmation: 'a new password'
                }
                user.reload
                expect(user.authenticate('a new password')).to eq(user)
            end
            it "should unset password_recovery_token" do 
                user
                post :update_password, {
                    token: user.password_recovery_token,
                    password: 'a new password',
                    password_confirmation: 'a new password'
                }
                user.reload
                expect(user.password_recovery_token).to be_nil 
                
            end
            it "should redirect to inventory_items_path" do 
                user
                post :update_password, {
                    token: user.password_recovery_token,
                    password: 'a new password',
                    password_confirmation: 'a new password'
                }
                expect(response).to redirect_to(inventory_landing_path)
            end
        end
        context "with invalid password" do
            it "should render reset_password" do 
                user
                post :update_password, {
                    token: user.password_recovery_token,
                    password: 'a new password',
                    password_confirmation: 'a new missmatched password'
                }
                expect(response).to render_template('reset_password')
            end
        end
        context "with invalid token" do 
            it "should render reset_password_template" do 
                user
                post :update_password, {
                    token: 'xxx',
                    password: 'a new password',
                    password_confirmation: 'a new password'
                }
                expect(response).to render_template('reset_form')
            end
        end
    end

end
