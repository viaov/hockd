require 'rails_helper'

RSpec.describe Homepage::SignupController, :type => :controller do
    let(:valid_attributes) {{
        user: {
            first_name: 'John',
            last_name: 'Doe',
            password: 'test123',
            password_confirmation: 'test123',
            email_attributes: { address: 'joe@example.com' },
            phone_number_attributes: { number: '4025557878' },
            location_attributes: {
                street_address: '123 N 10th',
                city: 'omaha',
                state: 'NE',
                postal_code: '12332'
            }

        },
        instance: {
            instance_name: 'test',
            company_attributes: {
                company_name: 'test',
                phone_number_attributes: {
                    number: '1234567890'
                },
                location_attributes: {
                    street_address: '123 N 113th St',
                    city: 'Omaha',
                    state: 'NE',
                    postal_code: '68116'
                }
            }
        }
    }}

    let(:invalid_attributes) {{
        user: {
            first_name: 'John',
            last_name: 'Doe',
            password: 'test123',
            password_confirmation: 'test123',
            email_attributes: { address: 'joe@example.com' },
            phone_number_attributes: { number: '4025557878' },
            location_attributes: {
                street_address: '123 N 10th',
                city: 'omaha',
                state: 'NE',
                postal_code: '12332'
            }

        },
        instance: {
            instance_name: 'test',
            company_attributes: {
                company_name: 'test',
                phone_number_attributes: {
                    number: '1234567890'
                },
                location_attributes: {
                    city: 'Omaha',
                    state: 'NE',
                    postal_code: '68116'
                }
            }
        }
    }}

    describe "GET #signup" do 
        it "assigns @instance" do
            get :signup
            expect(assigns(:instance)).to be_a_new(Instance)
        end
        it "builds company association on @instance" do 
            get :signup
            expect(assigns(:instance).company).to_not be_nil
        end
        it "builds @instance.company.location" do
            get :signup
            expect(assigns(:instance).company.location).to_not be_nil
        end
        it "builds @instance.company.phone_number" do 
            get :signup
            expect(assigns(:instance).company.phone_number).to_not be_nil
        end
    end
    describe "POST #do_signup" do 
        context "with valid attributes" do
            it "creates an instance" do 
                expect {
                    post :do_signup, valid_attributes
                }.to change(Instance, :count).by(1)
            end

            it "creates nested company" do 
                post :do_signup, valid_attributes
                expect(Instance.last.company).to_not be_nil
            end

            it "creates location for nested company" do 
                post :do_signup, valid_attributes
                expect(Instance.last.company.location).to_not be_nil
            end

            it "creates phone number for nested company" do 
                post :do_signup, valid_attributes
                expect(Instance.last.company.phone_number).to_not be_nil
            end
            it "should redirect to billing_path" do
                post :do_signup, valid_attributes 
                expect(response).to redirect_to(homepage_billing_path)
            end
        end
        context "without valid attributes" do 
            it "should render 'signup" do 
                post :do_signup, invalid_attributes
                expect(response).to render_template('signup')
            end
            it "should assign @instance" do 
                post :do_signup, invalid_attributes
                expect(assigns(:instance)).to be_a_new(Instance)
            end
        end
    end
    describe "POST #do_billing" do 
        let(:customer) { Stripe::Customer.new }
        let(:cards) { Object.new }
        let(:subscriptions) { Object.new }
        before(:each) do 
            expect(Stripe::Customer).to receive(:create).and_return(customer)
            allow(customer).to receive(:id).and_return('test1234')
            allow(customer).to receive(:cards).and_return(cards)
            expect(cards).to receive(:create).and_return(true)
            allow(customer).to receive(:subscriptions).and_return(subscriptions)
            expect(subscriptions).to receive(:create).and_return(true)
            allow_any_instance_of(AWS::SNS::Topic).to receive(:publish).and_return(true)
        end
        let(:user) { create(:user) }
        it "should send weclome email" do 
            expect {
                post :do_billing, {stripe_token: 'test123341'}, {user_id: user.id}
            }.to change(ActionMailer::Base.deliveries, :count).by(1)
        end
        it "should generate landing trigger" do 
            user
            expect {
                post :do_billing, {stripe_token: 'test123341'}, {user_id: user.id}
            }.to change(user.instance.landing_triggers, :count).by(1)
        end
    end
        
end
