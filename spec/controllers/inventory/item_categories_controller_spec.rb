require 'rails_helper'

RSpec.describe Inventory::ItemCategoriesController, :type => :controller do

    let(:instance) {create(:instance) }
    let(:item_category) {create(:item_category, instance_id: instance.id)}
    let(:valid_params) { {item_category: attributes_for(:item_category)} }
    let(:invalid_params) { {item_category: attributes_for(:item_category, name: nil)} } 


    before(:each) do 
        user = create(:user, instance_id: instance.id)
        allow(controller).to receive(:current_instance).and_return(instance)
        allow(controller).to receive(:current_user).and_return(user)
    end


    describe "#index" do 
        it "should render index" do 
            get :index
            expect(response).to render_template('index')
        end
        it "should assign @item_categories" do
            instance.item_categories.destroy_all
            item_categories = create_list(:item_category, 2, instance_id: instance.id)
            get :index
            expect(assigns(:item_categories)).to eq(item_categories)
        end
    end

    describe "#show" do 
        it "should assign @item_category" do 
            get :show, id: item_category.id
            expect(assigns(:item_category)).to eq(item_category)    
        end
    end

    describe "#new" do 
        it "should assign @item_category" do
            get :new
            expect(assigns(:item_category)).to be_a_new(ItemCategory)
        end
    end

    describe "#create" do 
        context "with valid params" do 
            it "should create item_category" do 
                expect {
                    post :create, valid_params 
                }.to change(ItemCategory, :count).by(1)
            end
            it "should redirect to #show" do 
                post :create, valid_params
                expect(response).to redirect_to [:inventory, ItemCategory.last]
            end
        end
        context "with invalid params" do 
            it "should not create item_category" do 
                expect {
                    post :create, invalid_params
                }.to change(ItemCategory, :count).by(0)
            end
            it "should render new" do 
                post :create, invalid_params
                expect(response).to render_template('new')
            end
            it "should assign @item_category" do 
                post :create, invalid_params
                expect(assigns(:item_category)).to be_a_new(ItemCategory)
            end
        end
    end

    describe "#edit" do 
        it "should assign @item_category" do 
            get :edit, id: item_category.id
            expect(assigns(:item_category)).to eq(item_category)
        end
    end

    describe "#update" do 
        context "with valid params" do 
            it "should update item_category" do 
                put :update, id: item_category.id, item_category: {name: 'test'}
                item_category.reload
                expect(item_category.name).to eq('test')
            end
            it "should redirect to #show" do 
                put :update, id: item_category.id, item_category: {name: 'test'}
                expect(response).to redirect_to [:inventory, item_category]
            end
        end
    end

    describe "#destroy" do 
        it "should destroy item_category" do 
            item_category
            expect {
                delete :destroy, id: item_category.id
            }.to change(ItemCategory, :count).by(-1)
        end
        it "should redirect to #index" do 
            delete :destroy, id:item_category.id
            expect(response).to redirect_to inventory_item_categories_path
        end
    end


end
