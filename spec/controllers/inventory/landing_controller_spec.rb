require 'rails_helper'

RSpec.describe Inventory::LandingController, :type => :controller do
    let(:instance) {create(:instance)}
    let(:user) {create(:user, instance_id: instance.id) }
    let(:landing_trigger) { 
        create(:landing_trigger, 
              landing_triggerable_id: instance.id, 
              landing_triggerable_type: 'Instance',
              view_name: 'getting_started')
    }
    before(:each) do
        allow(controller).to receive(:current_instance).and_return(instance)
        allow(controller).to receive(:current_user).and_return(user)
    end
    describe "#land" do 
        context "with trigger" do 
            before(:each) do
                user.landing_triggers.create(
                    landing_triggerable_id: instance.id,
                    landing_triggerable_type: 'Instance',
                    view_name: 'getting_started'
                )
            end
            it "should render triggered view" do 
               get :land
               expect(response).to render_template('getting_started')
            end
        end
        context "without trigger" do 
            it "should redirect to items path" do 
               get :land
               expect(response).to redirect_to(inventory_items_path)
            end
            
        end
    end


end
