require 'rails_helper'

RSpec.describe Inventory::BillingController, :type => :controller do
    let(:instance) { create(:instance) }
    let (:current_user) { create(:user, instance_id: instance.id) }

    before(:each) do
        allow(controller).to receive(:current_instance).and_return(instance)
        allow(controller).to receive(:current_user).and_return(current_user)
    end

    describe "#view" do 
        it "should call reload customer" do 
            expect(instance).to receive(:reload_customer)
            get :view
        end
        it "should render view template" do 
            allow(instance).to receive(:reload_customer)
            get :view
            expect(response).to render_template('view')
        end
    end

end
