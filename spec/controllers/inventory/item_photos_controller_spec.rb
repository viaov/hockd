require 'rails_helper'

RSpec.describe Inventory::ItemPhotosController, :type => :controller do

    let(:instance) { create(:instance) }
    let (:current_user) { create(:user, instance_id: instance.id) }
    let (:item) { create(:item,instance_id: instance.id) }

    before(:each) do
        allow(controller).to receive(:current_instance).and_return(instance)
        allow(controller).to receive(:current_user).and_return(current_user)
    end

    describe "#new" do 
        it "should render new template using modal layout" do 
            get :new, {item_id: item.id}
            expect(response).to render_template('new')
            expect(response).to render_template('layouts/modal')
        end
        it "shpould assign @item_photo" do 
            get :new, {item_id: item.id}
            expect(assigns(:item_photo)).to be_a_new(ItemPhoto)
        end
    end


end
