require 'rails_helper'

RSpec.describe Inventory::ItemsController, :type => :controller do

    let (:instance) {create(:instance) }
    let (:item) {create(:item, instance_id: instance.id)}
    let (:valid_attributes) { 
        attributes_for(:item, 
                       instance_id: instance.id,
                       item_category_id: 1, 
                       item_sub_category_id: 2) 
    }
    let (:invalid_attributes) { attributes_for(:item, title: nil) } 
    let (:current_user) { create(:user, instance_id: instance.id) }

    before(:each) do
        allow(controller).to receive(:current_instance).and_return(instance)
        allow(controller).to receive(:current_user).and_return(current_user)
    end

    describe "#index" do
        it "should assign @items, limited by instance" do 
            create(:item)
            items = create_list(:item, 2, instance_id: instance.id)
            get :index
            expect(assigns(:items).count).to eq(2)
        end 
    end

    describe "#show" do 
        it "should assign @item" do 
            get :show, id: item.id
            expect(assigns(:item)).to eq(item)
        end
    end

    describe "#new" do 
        it "should assign @item" do 
            get :new
            expect(assigns(:item)).to be_a_new(Item)
        end
    end

    describe "#create" do 
        context "with valid attributes" do 
            it "should create item" do 
                expect {
                    post :create, item: valid_attributes
                }.to change(instance.items, :count).by(1)
            end
            it "should redirect_to #show" do
                post :create, item: valid_attributes 
                expect(response).to redirect_to([:inventory, Item.last])
            end
        end
        context "with invlaid attributes" do 
            it "should not create item" do 
                expect {
                    post :create, item: invalid_attributes 
                }.to change(instance.items, :count).by(0)
            end
            it "should render new" do
                post :create, item: invalid_attributes 
                expect(response).to render_template('new') 
            end
        end
    end
    
    describe "#edit" do 
        it "should assign @item" do 
            get :edit, id: item.id
            expect(assigns(:item)).to eq(item)
        end
    end

    describe "#update" do 
        context "with valid attributes" do 
            it "should update item" do 
                put :update, id: item.id, item: attributes_for(:item, title: 'test')
                item.reload
                expect(item.title).to eq('test')
            end
            it "should redirect to #show" do 
                put :update, id: item.id, item: attributes_for(:item, title: 'test')
                item.reload
                expect(response).to redirect_to [:inventory, item]
            end
        end
        context "with invalid attributes" do 
            it "should not update item" do 
                put :update, id: item.id, item: attributes_for(:item, title: nil)
                item.reload
                expect(item.title).to_not be_nil
            end
            it "should render edit" do 
                put :update, id: item.id, item: attributes_for(:item, title: nil)
                expect(response).to render_template('edit')
            end
        end
    end



end
