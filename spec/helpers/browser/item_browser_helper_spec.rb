require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the Browser::ItemBrowserHelper. For example:
#
# describe Browser::ItemBrowserHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe Browser::ItemBrowserHelper, :type => :helper do
    let(:item) { create(:item) } 
    describe "#featured_item" do
        it "should render featured_item partial" do 
            expect(helper).to receive(:render).with(
                partial: 'browser/featured_item', 
                object: item
            )
            helper.featured_item(item)
        end 
    end

    describe "#item_display_style" do 
        it "should return param d" do 
            allow(helper).to receive(:params).and_return({
                d: 'test'
            })
            expect(helper.item_display_style).to eq("test")
        end
        it "should return list if not set" do 
            expect(helper.item_display_style).to eq("list")
        end
    end


    describe "#filter_link" do 

    end


end
