require 'rails_helper'



feature "user signin" do 
    given(:user) { create(:user, password: 'test12345', password_confirmation: 'test12345') }
    scenario "with valid credentials" do 
        visit homepage_signin_path 
        fill_in :email, with: user.email.address
        fill_in :password, with: 'test12345'
        click_button 'Login'
        expect(current_path).to eq(inventory_items_path)
    end
end
