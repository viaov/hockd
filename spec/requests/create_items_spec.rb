require 'rails_helper'


feature "create new item" do 
    scenario "with valid attributes" do
        user = login_user
        category = create(:item_category, instance_id: user.instance.id)
        sub_category = create(:item_sub_category, item_category_id: category.id)
        visit new_inventory_item_path
        item = build(:item)
        fill_in :item_title, with: item.title
        fill_in :item_description, with: item.description
        fill_in :item_price, with: item.price
        select item.condition, from: :item_condition
        select category.name, :from => :item_item_category_id
        find('input#item_item_sub_category_id').set sub_category.id
        expect {
            click_on 'Create Item'
        }.to change(user.instance.items, :count).by(1)
        expect(current_path).to eq(inventory_item_path(Item.last))
    end
end

feature "edit item" do 
    scenario "successful edit" do 
        instance = create(:instance)
        user = login_user(instance)
        create(:item, instance_id: instance.id)
        visit edit_inventory_item_path(Item.last)
        expect(current_path).to eq(edit_inventory_item_path(Item.last))
        fill_in :item_title, with: 'test item title' 
        click_on 'Update Item'
        expect(current_path).to eq(inventory_item_path(Item.last))
        expect(page).to have_content('test item title')
    end
end

feature "delete an item", js: true  do 
    scenario "successful delete" do 
        instance = create(:instance)
        user = login_user(instance)
        create(:item, instance_id: instance.id)
        visit inventory_item_path(Item.last)
        expect {
            accept_confirm { find(%Q|a[data-method=delete]|).click }
        }.to change(Item, :count).by(-1)
    end
end
