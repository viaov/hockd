require 'rails_helper'

feature "Display landing page" do 
    scenario "Instance landing trigger" do
        user = create(:user)
        user.instance.landing_triggers.create(view_name: 'getting_started')
        visit homepage_signin_path
        fill_in :email, with: 'test@example.com'
        fill_in :password, with: 'test123'
        click_button 'Login'
        expect(current_path).to eq(inventory_landing_path)
        expect(page).to have_content('Thanks for choosing Hockd')
        click_link 'Continue'
        expect(current_path).to eq(inventory_items_path)
        click_link 'Logout'
        fill_in :email, with: 'test@example.com'
        fill_in :password, with: 'test123'
        click_button 'Login'
        expect(current_path).to eq(inventory_items_path)
    end
end
