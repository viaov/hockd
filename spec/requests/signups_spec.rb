require 'rails_helper'



feature "signing up" do

    scenario "successful signup" do 
        visit homepage_signup_path

        fill_in :instance_instance_name, with: attributes_for(:instance)[:instance_name]
        fill_form(:user, attributes_for(:user).slice(*:first_name, :last_name))
        fill_in :user_password, with: attributes_for(:user)[:password]
        fill_in :user_password_confirmation, with: attributes_for(:user)[:password]
        fill_in :user_phone_number_attributes_number, with: attributes_for(:phone_number)[:number]
        fill_in :user_email_attributes_address, with: attributes_for(:email)[:address]
        fill_form(:company, attributes_for(:company))
        fill_form(:location, attributes_for(:location).slice(*:street_address, :city, :postal_code)) 
        select 'Nebraska', :from => :instance_company_attributes_location_attributes_state
        fill_form(:phone_number, attributes_for(:phone_number).slice(:number))

        expect {
            click_button 'Continue'
        }.to change(Instance, :count).by(1)
        expect(Instance.last.company.phone_number.number).to eq(attributes_for(:phone_number)[:number])
        expect(current_path).to eq(homepage_billing_path)
    end

end

