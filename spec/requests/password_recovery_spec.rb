require 'rails_helper'

feature "password recovery" do 
    scenario "successfuly recovery" do 
        user = create(:user)
        visit homepage_landing_path
        click_link 'Login'
        click_link 'Recover password'
        fill_in :email, with: user.email.address
        click_button 'Recover'
        user.reload
        visit homepage_reset_password_url(token: user.password_recovery_token)
        fill_in :password, with: 'test101010'
        fill_in :password_confirmation, with: 'test101010'
        click_button 'Reset Password'
        click_link 'Logout'
        fill_in :email, with: user.email.address
        fill_in :password, with: 'test101010'
        click_button 'Login'
        expect(current_path).to eq(inventory_items_path)
    end
end
