# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :apperance_setting do
      instance
      theme_name 'test'
      scheme_name 'test'
  end
end
