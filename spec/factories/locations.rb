# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :location do
    street_address "123 CoolCreek Rd"
    city "Omaha"
    state "NE"
    postal_code "68116"
    latitude nil
    longitude nil 
  end
end
