# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :item_category do
    sequence(:name) {|n| "Category#{n}"}
    instance
  end
end
