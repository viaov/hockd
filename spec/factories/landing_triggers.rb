# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
    factory :landing_trigger do
        view_name 'getting_started'
        auto_remove true
        presented false
        forceful false
    end
end
