# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :phone_number do
    number "4025551212"
    number_type "landline"
  end
end
