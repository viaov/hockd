# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
    factory :instance do
        sequence :instance_name do |n| 
            "myinstance#{n}"
        end
        company
    end
end
