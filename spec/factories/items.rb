# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
    factory :item do
        instance
        item_category
        item_sub_category
        title "MyString"
        description "MyString"
        condition 'New'
        price 2.99
        featured false
        factory :item_featured do 
            featured true
        end
    end
end
