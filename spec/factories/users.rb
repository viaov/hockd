# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    first_name "John"
    last_name "Doe"
    password 'test123'
    password_confirmation 'test123'
    email
    location
    phone_number
    instance
  end
end
