# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company do
    company_name "Pawn Palace"
    location
    phone_number
  end
end
