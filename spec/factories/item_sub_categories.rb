# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
    factory :item_sub_category do
        item_category
        name "MyString"
    end
end
