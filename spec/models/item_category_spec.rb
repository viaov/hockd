require 'rails_helper'

RSpec.describe ItemCategory, :type => :model do

    let(:item_category) { create(:item_category) }
    
    it "should not be valid without name" do
        expect(build(:item_category, name: nil)).to_not be_valid
    end

    it "should not be vlaid without instance" do
        expect(build(:item_category, instance_id: nil)).to_not be_valid
    end

    it "should delete sub_categories  when destroying category" do 
        item_category
        item_sub_category = create(:item_sub_category, item_category_id: item_category.id)
        expect {
            item_category.destroy
        }.to change(ItemSubCategory, :count).by(-1)
    end

end
