require 'rails_helper'

RSpec.describe Company, :type => :model do

    it "should not be valid without a name" do
        expect(build(:company, company_name: nil)).to be_invalid
    end

end
