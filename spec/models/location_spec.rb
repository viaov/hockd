require 'rails_helper'

RSpec.describe Location, :type => :model do

    it "should not be valid without street address" do
        expect(build(:location, street_address: nil)).to be_invalid
    end
    
    it "should not be valid without a city" do 
        expect(build(:location, city: nil)).to be_invalid
    end
    
    it "should not be valid without a state" do
        expect(build(:location, state: nil)).to be_invalid
    end

    it "should not be valid wihtout a postal code" do
        expect(build(:location, postal_code: nil)).to be_invalid
    end

    it "should not geocode during test" do
        expect(create(:location).latitude).to be_nil 
    end

end
