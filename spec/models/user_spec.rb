require 'rails_helper'

RSpec.describe User, :type => :model do
    let(:user) { build(:user, password: '123456', password_confirmation: '123456') } 

    it "should not be valid without first_name" do 
        expect(build(:user, first_name: nil)).to be_invalid
    end
    it "should not be valid without last_name" do 
        expect(build(:user, last_name: nil)).to be_invalid
    end
    it "should not be valid without password" do 
        expect(build(:user, password: nil)).to be_invalid
    end
    it "Should not be valid without password confirmation" do 
        expect(build(:user, password: 'test12345', password_confirmation: 'test1234566')).to be_invalid
    end

    describe "#authenticate" do 
        it "should return true for valid password" do
            expect(user.authenticate('123456')).to eq(user)
        end
        it "should return false for invalid password" do 
            expect(user.authenticate('1234567')).to eq(false)
        end
    end

    describe "::authenticate" do 
        it "should return User for valid credientials" do
            user.save
            expect(User.authenticate(user.email.address, '123456')).to eq(user)
        end

        it "should return nil for invlaid credentials" do 
            user.save
            expect(User.authenticate(user.email.address, 'nope')).to eq(false)
        end
    end

    describe "has_triggers?" do 
        it "should return true with waiting triggers" do 
            triggering_user = create(:user)
            trigger = create(:landing_trigger, 
                             landing_triggerable_id: triggering_user.id, 
                             landing_triggerable_type: 'User')
            expect(triggering_user.has_triggers?).to eq(true)
        end
    end



    

end
