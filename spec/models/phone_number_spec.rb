require 'rails_helper'

RSpec.describe PhoneNumber, :type => :model do
    it "should not be valid without number" do 
        expect(build(:phone_number, number: nil)).to be_invalid
    end
end
