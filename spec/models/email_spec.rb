require 'rails_helper'


RSpec.describe Email, :type => :model do
    it "should not be valid without address" do 
        expect(build(:email, address: nil)).to be_invalid
    end
end
