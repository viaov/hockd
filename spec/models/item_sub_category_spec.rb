require 'rails_helper'

RSpec.describe ItemSubCategory, :type => :model do
    it "should be invalid without name" do 
        expect(build(:item_sub_category, name: nil)).to be_invalid
    end
end
