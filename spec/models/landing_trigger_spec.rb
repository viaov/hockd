require 'rails_helper'

RSpec.describe LandingTrigger, :type => :model do

    it "should not be valid without view_name" do 
        expect(build(:landing_trigger, view_name: nil)).to be_invalid
    end
    describe "#should_present?" do 
        it "should present if not presented" do 
            expect(build(:landing_trigger, presented: false).should_present?).to eq(true)
        end

        it "should present if forceful" do 
            expect(build(:landing_trigger, presented: true, forceful: true).should_present?).to eq(true)
        end

        it "should not present if already presented" do 
            expect(build(:landing_trigger, presented: true).should_present?).to eq(false)
        end
    end    
    describe "#auto_remove!" do 
        it "should destroy if auto_removable" do 
            trigger = build(:landing_trigger, auto_remove:true)
            expect(trigger).to receive(:destroy) 
            trigger.auto_remove!
        end 
        it "should not destroy if not auto_removable" do 
            trigger = build(:landing_trigger, auto_remove:false)
            expect(trigger).to_not receive(:destroy) 
            trigger.auto_remove!
        end 
    end

    describe "presentable scope" do 
        it "should return unpresented and forceful triggers" do 
            unpresented_trigger = create(:landing_trigger)
            presented_trigger = create(:landing_trigger, presented: true)
            forceful_trigger = create(:landing_trigger, presented: true, forceful: true)
            expect(LandingTrigger.presentable).to eq([unpresented_trigger, forceful_trigger])
        end
    end

end
