require 'rails_helper'

RSpec.describe Instance, :type => :model do

    it "should not be valid without name" do
        expect(build(:instance, instance_name: nil)).to_not be_valid 
    end


    it "should automatically generate an identifier" do 
        expect(create(:instance, instance_name: 'test').identifier).to_not be_nil
    end


    it "should not be valid with an instance name less than three characters" do 
        expect(build(:instance, instance_name: 'aa')).to be_invalid
    end

    it "#instance_name should be unique" do 
        create(:instance, instance_name: 'test')
        expect(build(:instance, instance_name: 'test')).to be_invalid
    end

    it "should create an apperance_setting record" do
        instance = create(:instance) 
        expect(instance.apperance_setting).to be_present
    end


    it "should automatically generate default categories" do 
        instance = create(:instance)
        expect(instance.item_categories.count).to_not eq(0)
        instance.item_categories.each do |category|
            expect(category.item_sub_categories.count).to_not eq(0)
        end
    end

    it "should force #instance_name to lowercase" do 
        instance = create(:instance, instance_name: 'TEST') 
        expect(instance.instance_name).to eq('test')
    end

    describe "has_triggers?" do 
        it "should return true with waiting triggers" do 
            triggering_instance = create(:instance)
            trigger = create(:landing_trigger, 
                             landing_triggerable_id: triggering_instance.id, 
                             landing_triggerable_type: 'Instance')
            expect(triggering_instance.has_triggers?).to eq(true)
        end
    end

end
