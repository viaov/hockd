require 'rails_helper'

RSpec.describe Item, :type => :model do
    let(:item) { create(:item) } 
    it "should not be valid without title" do 
        expect(build(:item, title: nil)).to be_invalid
    end

    describe "#attach_photo" do 
        it "should create item_photo record" do 
            item
            allow_any_instance_of(Paperclip::Attachment).to receive(:save).and_return(true)
            expect {
               item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'xbox.jpg'))
            }.to change(item.photos, :count).by(1)
        end
        it "should attach photo to item_photo record" do 
            allow_any_instance_of(Paperclip::Attachment).to receive(:save).and_return(true)
           item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'xbox.jpg'))
           expect(item.photos.first.image).to_not be_nil
        end
    end
    
    describe "#has_photos?" do 
        it "should return true for item with photos" do 
            allow_any_instance_of(Paperclip::Attachment).to receive(:save).and_return(true)
           item.attach_photo(File.join(Rails.root, 'spec', 'test_photos', 'xbox.jpg'))
           expect(item.has_photos?).to eq(true)
        end
        it "should return false for item without photos" do 
            expect(item.has_photos?).to eq(false)
        end
    end
end
