require 'rails_helper'

RSpec.describe ApperanceSetting, :type => :model do

    it "should not be valid without theme_name" do 
        expect(build(:apperance_setting, theme_name: nil)).to be_invalid
    end
    it "should not be valid wihtout scheme_name" do 
        expect(build(:apperance_setting, scheme_name: nil)).to be_invalid
    end

    describe "#has_logo?" do 
        it "should return true if an image is attached" do 
            setting = create(:apperance_setting)
            setting.logo_image = File.open(Rails.root.join('spec', 'test_photos', 'xbox.jpg'))
            expect(setting.has_logo?).to eq(true)
        end
        it "should return false without an image attached" do
            setting = create(:apperance_setting)
            expect(setting.has_logo?).to eq(false)
        end
    end
    
   describe "#full_size_logo" do 
       it "should return true when full_size_logo = true" do 
           setting = build(:apperance_setting, full_size_logo: true)
           expect(setting.full_size_logo?).to eq(true)
       end
       it "should return false when full_size_logo = false" do 
           setting = build(:apperance_setting, full_size_logo: false)
           expect(setting.full_size_logo?).to eq(false)
       end
   end 
    
end
