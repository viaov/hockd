class ItemSubCategory < ActiveRecord::Base
    belongs_to :item_category, class_name: 'ItemCategory', foreign_key: 'item_category_id'
    validates :name, presence: { message: 'Categories must have a name.' }, uniqueness: { scope: 'item_category_id', message: 'A subcategory with that name already exists for its parent category.' }
    has_many :items, :dependent => :destroy



    def to_s 
        name
    end


    
end
