class ApperanceSetting < ActiveRecord::Base
    belongs_to :instance
    has_attached_file :logo_image, 
        styles: { full: '1000', thumb: '400x100', logo: '400x80' },
        default_url: "images/:style/missing.png",
        :storage => :s3,
        :s3_credentials => Rails.root.join('config','item_photos_s3_config.yml')
    validates_attachment_content_type :logo_image, :content_type => /\Aimage\/.*\Z/

    validates :theme_name, presence: true
    validates :scheme_name, presence: true 



    def has_logo?
        logo_image.present?
    end

    def full_size_logo?
        full_size_logo
    end
end
