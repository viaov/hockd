class LandingTrigger < ActiveRecord::Base
    belongs_to :landing_triggerable, polymorphic: true
    validates :view_name, presence: true

    scope :presentable, -> {where('presented = false OR forceful = true')}

    def should_present?
        forceful || !presented
    end

    def auto_remove!
        destroy if auto_remove
    end

    def presented!
        update(presented: true, presented_at: Time.current) unless auto_remove
        auto_remove!
    end
    
end
