class Location < ActiveRecord::Base

    geocoded_by :address 
    after_validation :geocode, :if => :should_geocode


    validates :street_address, presence: { message: 'Enter a street address.' }
    validates :city, presence: { message: 'Enter a city.' }
    validates :state, presence: { message: 'Enter a state.' }, length: {is: 2, message: 'States must be entered using their two character abbreviations.' }
    validates :postal_code, presence: { message: 'Enter a postal code.' }


    def address
        "#{street_address} #{city}, #{state} #{postal_code}"
    end

    def should_geocode
        Rails.env != 'test'
    end

end
