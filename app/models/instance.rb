class Instance < ActiveRecord::Base
    validates :instance_name, presence: { message: 'Please enter an application name.'}, 
        format: { with: /\A[A-Za-z0-9]+\Z/, message: 'Instance names may not contain spaces or special characters.' },
        length: {minimum: 3, message: 'The application name must be atleast 3 characters long.'}, 
        uniqueness: {message: 'This name has already been taken. Please select a different name.' } 
    validates_associated :company

    has_many :item_categories, :dependent => :destroy
    has_many :item_sub_categories, :through => :item_categories
    has_one :apperance_setting, :dependent => :destroy
    has_many :items
    belongs_to :company, :dependent => :destroy
    has_many :users, :dependent => :destroy
    has_many :landing_triggers, :as => :landing_triggerable

    accepts_nested_attributes_for :company 


    before_create :generate_identifier
    before_create {|i| i.build_apperance_setting}
    before_save { |instance| instance.instance_name = instance.instance_name.downcase }

    after_create :create_default_categories

    before_destroy :close_stripe_customer

    def generate_identifier
        self.identifier = SecureRandom.uuid
    end
    def reload_customer
        @customer = Stripe::Customer.retrieve(stripe_token)
    end
    def customer 
        unless stripe_token
            @customer =  Stripe::Customer.create(description: company.company_name)
            self.update(stripe_token: @customer.id)
        end
        begin
            @customer ||= Stripe::Customer.retrieve(stripe_token)
        rescue Stripe::InvalidRequestError
            @customer = Stripe::Customer.create(description: company.company_name)
            self.update(stripe_token: @customer.id)
            return @customer
        end
    end

    def default_card
        return nil unless customer.default_card
        customer.cards.retrieve(customer.default_card)
    end

    def close_stripe_customer
        customer.delete
    end

    def create_default_categories
        item_categories.create(name: 'Electronics').item_sub_categories.create([{name: 'Entertainments'}, {name: 'Home Audio'}, {name: 'Car Audio'}])
        item_categories.create(name: 'Jewelry').item_sub_categories.create([{name: 'Rings'}, {name: 'Necklaces'}, {name: 'Earrings'}])
        item_categories.create(name: 'Firearms').item_sub_categories.create([{name: 'Handguns'}, {name: 'Riffles'}, {name: 'Ammunition'}])
        item_categories.create(name: 'Musical Instruments').item_sub_categories.create([{name: 'Instruments'}, {name: 'Accessories'}])
        item_categories.create(name: 'Antiques').item_sub_categories.create([{name: 'Antique Furniture'}, {name: 'Antique Decor'}])
    end

    def has_triggers?
        landing_triggers.presentable.count > 0 
    end


end
