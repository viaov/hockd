class ItemPhoto < ActiveRecord::Base
    belongs_to :item
    has_attached_file :image, 
        styles: { large: '600', medium: '300', small: '200x200#', thumb: '100x100#', tiny: 'x32'}, 
        default_url: "images/:style/missing.png",
        :storage => :s3,
        :s3_credentials => Rails.root.join('config','item_photos_s3_config.yml')

    validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

    
end
