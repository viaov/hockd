class ItemCategory < ActiveRecord::Base
    belongs_to :instance
    has_many :item_sub_categories, :dependent => :destroy
    has_many :items, :through => :item_sub_categories
    validates :name, presence: {message: 'Catagories must have a name.'}, uniqueness: {:scope => :instance_id, message: 'Another category with that name already exists.'}
    validates :instance_id, presence: true
    scope :popular, ->{joins(:items).group('item_categories.id').order('count(items.id) DESC')}




    def to_s
        name
    end

end
