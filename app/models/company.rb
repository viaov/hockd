class Company < ActiveRecord::Base
    belongs_to :phone_number, :dependent => :destroy
    belongs_to :location, :dependent => :destroy

    validates :company_name, presence: { message: 'Enter a company name.' }

    accepts_nested_attributes_for :location
    accepts_nested_attributes_for :phone_number
end
