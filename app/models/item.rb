class Item < ActiveRecord::Base
    has_many :photos, class_name: 'ItemPhoto'
    belongs_to :item_category
    belongs_to :item_sub_category
    belongs_to :instance
    validates :title, presence: {message: 'Please enter a title for this item.'}
    validates :price, numericality: { message: 'Prices must be entered as numbers.'}, presence: true
    validates :item_sub_category_id, presence: {message: 'Items must belong to a sub-category.'}
    validates :item_category_id, presence: { message: 'Items must have a category.' }


    scope :featured, -> { where(featured: true) }

    def has_photos?
        photos.count > 0
    end

    def primary_photo 
        @primary_photo ||= photos.find_by_primary(true) || photos.first
    end

    def primary_photo_url(style = :medium)
        if primary_photo
            primary_photo.image.url(style) 
        else
            'no_image_available.jpg'
        end
    end

    def primary_photo_size(style = :medium)
        primary_photo.try(:image).try(:image_size, style)
    end

    def category
        item_sub_category.item_category
    end

    def attach_photo(file_path)
        return nil unless File.exists?(file_path)
        item_photo = photos.new
        image_file = File.open(file_path)
        item_photo.image = image_file 
        image_file.close
        item_photo.save
    end

    def related_items(number_of_records = 5)
        Item.where(item_category: item_category, item_sub_category_id: item_sub_category.id).where.not(id: id).order('featured desc, view_count desc').limit(number_of_records)
    end

    def has_related_items?
        related_items.count > 0 
    end

    def to_param
        "#{id} #{title}".parameterize
    end


end
