class PhoneNumber < ActiveRecord::Base
    validates :number, presence: {message: 'A phone number is requried.'}, numericality: {message: 'Phone numbers should be entered using only numbers.'}



    def to_s
       number 
    end

end
