class Email < ActiveRecord::Base
    validates :address, presence: { message: 'Enter an email address.' }, uniqueness: { message: 'This email address has already been used to create an account.' }
end
