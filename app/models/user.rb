
class User < ActiveRecord::Base
    has_secure_password 

    belongs_to :phone_number, :dependent => :destroy
    belongs_to :email, :dependent => :destroy
    belongs_to :instance
    belongs_to :location, :dependent => :destroy
    has_many :landing_triggers, :as => :landing_triggerable

    validates :first_name, presence: { message: 'Enter a first name.' }
    validates :last_name, presence: { message: 'Enter a last name.' }
    validates :password, length: { minimum: 6, message: 'Password is too short.' }, allow_nil: true


    accepts_nested_attributes_for :email
    accepts_nested_attributes_for :location
    accepts_nested_attributes_for :phone_number

    def self.authenticate email,password
        User.joins(:email).find_by(emails: { address: email}).try(:authenticate, password)
    end

    def to_s
        "#{first_name} #{last_name}"
    end

    def name
        "#{first_name} #{last_name}"
    end

    def has_triggers?
        landing_triggers.presentable.count > 0 
    end

end
