class UserPreview < ActionMailer::Preview
    def welcome
        UserMailer.welcome(User.first)
    end
    def reset_password
        user = FactoryGirl.build(:user, password_recovery_token: SecureRandom.hex(20))
        UserMailer.reset_password(user)
    end

end
