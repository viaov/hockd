class UserMailer < ActionMailer::Base
    default from: "noreply@hockd.com"
    add_template_helper MailHelper

    def welcome user
        @user = user
        attachments.inline['logo.png'] = File.read(Rails.root.join('app','assets','images','maiL_logo.png'))
        mail to: user.email.address, subject: 'Welcome to Hockd!'
    end

    def reset_password(user)
        @user = user 
        attachments.inline['logo.png'] = File.read(Rails.root.join('app','assets','images','maiL_logo.png'))
        mail to: @user.email.address, subject: 'Hockd - Password Recovery' 
    end

end
