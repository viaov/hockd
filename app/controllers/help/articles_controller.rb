class Help::ArticlesController < Help::BaseController

    before_action :validate_article, only: [:article]

    def landing 
        redirect_to help_article_path('getting_started')
    end

    def article
        render params[:article]
    end

    private

    def validate_article
        return render 'not_found' unless valid_article?
    end

    def valid_article?
       %w{getting_started domain categories item_photos related featured payment_security}.include? params[:article]
    end

end
