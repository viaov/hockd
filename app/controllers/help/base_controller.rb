class Help::BaseController < ApplicationController 
    layout 'help'


    private 

    def logged_in?
        User.exists?(id: session[:user_id]) 
    end
    helper_method :logged_in?

    def current_instance
        return nil unless current_user
        @current_instance ||= current_user.instance
    end 
    helper_method :current_instance 

    def current_user 
        return nil unless logged_in?
        @current_user ||= User.find(session[:user_id])
    end
    helper_method :current_user
end
