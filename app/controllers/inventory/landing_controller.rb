class Inventory::LandingController < Inventory::InventoryController
    layout 'landing_trigger'
    before_action :set_trigger

    def land
        return render params[:view] if params[:view]
        if @current_trigger
            return render @current_trigger.view_name
        end 
        redirect_to inventory_items_path
    end

    def dissmiss
        @current_trigger.destroy
        redirect_to inventory_landing_path
    end

    private 

    def set_trigger
        @current_trigger ||= (current_user.landing_triggers.presentable.first || current_instance.landing_triggers.presentable.first)
    end


end
