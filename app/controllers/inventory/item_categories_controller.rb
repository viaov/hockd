class Inventory::ItemCategoriesController < Inventory::InventoryController

    before_action :set_item_category, only: [:show, :edit, :update, :destroy]

    def index
        @q = current_instance.item_categories.order(:name).search(params[:q])
        @item_categories = @q.result.page(page).per(per)
    end

    def show

    end

    
    def new
        @item_category = current_instance.item_categories.new
    end

    def create 
       @item_category = current_instance.item_categories.new(item_category_params)
      if @item_category.save
         redirect_to [:inventory, @item_category]
      else 
         render 'new' 
      end
    end

    def edit
    end

    def update
        if @item_category.update(item_category_params)
            redirect_to [:inventory, @item_category]
        else
            render 'edit'
        end
    end
       

    def destroy 
        @item_category.destroy
        redirect_to inventory_item_categories_path
    end


    private

    def set_item_category 
        @item_category = current_instance.item_categories.find_by_id(params[:id])
    end

    def item_category_params 
        params.require(:item_category).permit(:name)
    end
    
    def page 
        params[:page] || 0
    end
    
    def per
        params[:per] || 10
    end

end
