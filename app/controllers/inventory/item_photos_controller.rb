class Inventory::ItemPhotosController < Inventory::InventoryController

    before_action :set_item
    before_action :set_item_photo, only: [:set_primary, :destroy]

    def new
        @item_photo = @item.photos.new
        render layout: 'modal'
    end

    def create
       return redirect_to [:inventory, @item] unless params[:item_photo]
       @item.photos.new(params.require(:item_photo).permit(:image)) 
       if @item.save
           redirect_to [:inventory, @item]
       end
    end

    def destroy
        @item_photo.destroy
        redirect_to [:inventory, @item]
    end

    def set_primary
        ItemPhoto.where(item: @item).update_all(primary: false)
        @item_photo.update(primary:true)
        redirect_to [:inventory, @item]
    end

    def set_item
        @item = current_instance.items.find(params[:item_id])
    end
    
    def set_item_photo
        @item_photo = @item.photos.find(params[:id])
    end

end
