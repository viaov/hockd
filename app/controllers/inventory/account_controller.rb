class Inventory::AccountController < Inventory::InventoryController
    def show
        current_user.build_location unless current_user.location
    end
    def update_user 
        if current_user.update(user_params) 
            flash.now[:notice] = 'User information saved'
        end
        render 'show'
    end

    private

    def user_params
        params.require(:user).permit(
            :first_name, 
            :last_name, 
            :password,
            :password_confirmation,
            :email => [:address],
            :location => [:city, :state, :postal_code]
        )
    end
end
