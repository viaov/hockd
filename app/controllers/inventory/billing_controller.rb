class Inventory::BillingController < Inventory::InventoryController
    if Rails.env.production?
        force_ssl
    end    

    def view
        current_instance.reload_customer
    end

    def new_card
    end

    def create_card
        current_instance.customer.cards.create(card: params[:stripe_token])
        redirect_to inventory_billing_path
    end

    def make_primary 
        current_instance.customer.default_card = params[:card_token]
        current_instance.customer.save
        redirect_to inventory_billing_path
    end

    def delete_card
        if current_instance.customer.cards.count == 1 
            flash[:notice] = "You must add an additional card before removing the last one."
        else
            current_instance.customer.cards.retrieve(params[:card_token]).delete
        end
        redirect_to inventory_billing_path
    end

    def cancel_account
        current_instance.update(cancelled: true, cancellation_reason: params[:reason])
        current_instance.customer.subscriptions.first.try(:delete)
        redirect_to inventory_billing_path
    end

end
