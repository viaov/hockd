class Inventory::InventoryController < ApplicationController 
    layout 'inventory' 

    before_action :check_instance

    private
    def check_instance
        redirect_to homepage_signin_path unless current_instance && current_user
    end

    def current_instance 
        @current_instance ||= current_user.try(:instance)
    end
    helper_method :current_instance

    def current_user
        @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end
    helper_method :current_user

end
