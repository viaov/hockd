class Inventory::ItemSubCategoriesController < Inventory::InventoryController 
    respond_to :html, :json

    before_action :set_item_category 
    before_action :set_item_sub_category, :only => [:show, :destroy, :edit, :update]


    def index
        @item_sub_categories = @item_category.item_sub_categories
        respond_with(@item_sub_categories)
    end


    def show

    end

    def new
        @item_sub_category = ItemSubCategory.new
    end

    def create
        @item_sub_category = @item_category.item_sub_categories.new(sub_category_params)
        if @item_sub_category.save
            redirect_to [:inventory, @item_category, @item_sub_category]
        else
            render 'new'
        end
    end

    def edit

    end

    def update
        if @item_sub_category.update(sub_category_params)
            redirect_to [:inventory, @item_category, @item_sub_category]
        else
            render 'edit'
        end
    end

    def destroy
        @item_sub_category.destroy
        redirect_to [:inventory, @item_category]
    end


    private

    def set_item_category
        @item_category = current_instance.item_categories.find_by_id(params[:item_category_id])
    end

    def set_item_sub_category 
        return nil unless @item_category
        @item_sub_category = @item_category.item_sub_categories.find_by_id(params[:id])
    end

    def sub_category_params
        params.require(:item_sub_category).permit(:name)
    end

end
