class Inventory::SettingsController < Inventory::InventoryController
    def application_settings
        @instance = current_instance
        @company = current_instance.company
    end

    def update_application_settings
        @instance = current_instance
        if @instance.update(instance_params)
            flash.now[:notice] = 'Information updated successfully.'
        end
        render 'application_settings'
    end


    def apperance_settings
    end

    def remove_logo 
        current_instance.apperance_setting.update(logo_image: nil)
        redirect_to inventory_apperance_settings_path
    end

    def update_apperance_settings
        if current_instance.apperance_setting.update(apperance_params)
            flash.now[:notice] = 'Settings updated.'
        else 
            flash.now[:notice] = "Settings could not be updated."
        end
        render 'apperance_settings'
    end


    private 

    def instance_params
        params.require(:instance).permit( 
            :instance_name,
            company_attributes: [
                :company_name, 
                phone_number_attributes: [:number],
                location_attributes: [:street_address, :city, :state, :postal_code]
        ]
        )
    end


    def apperance_params
        params.require(:apperance_setting).permit(:scheme_name, :theme_name, :logo_image, :full_size_logo)
    end

end
