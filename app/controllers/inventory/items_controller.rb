class Inventory::ItemsController < Inventory::InventoryController

    before_action :set_item, only: [:show, :edit, :update, :destroy, :new_photo, :create_photo, :delete_photo]


    def index
        @search = current_instance.items.order(:id => :desc).search(params[:q])
        @items = @search.result.page(page).per(per)
    end

    def new
        @item = current_instance.items.new
    end

    def create
        @item = current_instance.items.new(item_params)
        if @item.save
            redirect_to [:inventory, @item]
        else
            render 'new'
        end
    end

    def edit
    end

    def update
        if @item.update(item_params)
            redirect_to [:inventory, @item]
        else
            render 'edit'
        end
    end

    def destroy
        @item.destroy
        redirect_to inventory_items_path
    end

    private

    def set_item 
        @item = current_instance.items.find(params[:id])
    end

    def item_params 
        item_params  = params.require(:item).permit(:title, :identifier, :description, :item_sub_category_id, :price, :item_category_id, :featured)
        item_params[:properties] = item_properties
        item_params

    end

    def item_properties 
        properties_hash = {}
        if params[:properties]
            params[:properties].each do |property| 
                properties_hash[property['property_name'].strip] = property['property_value'].strip if property['property_name'].present? && property['property_value'].present?
            end
        end
        properties_hash
    end
    
    def page
        params[:page] || 0
    end

    def per
        params[:per] || 15
    end

end
