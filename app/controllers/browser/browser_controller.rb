class Browser::BrowserController < ApplicationController 
    layout :get_layout

    before_action :theme_view_path 

    private

    def current_instance
        @instance ||= Instance.find_by_instance_name(request.subdomain)
    end
    helper_method :current_instance

    def current_theme
        @theme ||= current_instance.apperance_setting.theme_name
    end
    helper_method :current_theme

    def get_layout
        "browser/#{current_theme}"
    end

    def theme_view_path
        prepend_view_path "app/views/themes/#{current_theme}"
    end

end
