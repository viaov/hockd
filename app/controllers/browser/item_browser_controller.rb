class Browser::ItemBrowserController < Browser::BrowserController
    

    def land
        return render 'no_items' if current_instance.items.count == 0
        @items = current_instance.items.search(params[:q]).result.page(page).per(per)
        @hero_item = current_instance.items.order('featured desc, view_count desc').limit(1).first
        @featured_items = current_instance.items.order('featured desc, view_count desc').limit(24).offset(1)
        @featured_items = @featured_items.limit(@featured_items.count - ( @featured_items.count % 4)) if @featured_items.count < 24 && @featured_items.count > 4
    end


    def search
        @items = handle_filters(current_instance.items).search(params[:q]).result.page(page).per(per)
        @sub_categories = ItemSubCategory.where(id: current_instance.items.search(params[:q]).result.select(:item_sub_category_id)) 
        @title = "Searching for \"#{params[:q]['title_cont']}\"" if params[:q]
        render 'show_items'
    end


    def show_item
        @item = current_instance.items.find(params[:id])
        @item.update(last_viewed_at: Time.current, view_count: @item.view_count+1)
    end



    def browse_category 
       @category = current_instance.item_categories.find(params[:id])
       @items = handle_filters @category.items
       @sub_categories = @category.item_sub_categories
       @display = params[:d] || 'grid'
       @title = "Browseing #{@category}"
       render 'show_items'
    end

    private


    def handle_filters(items)
       items = items.where(item_sub_category_id: params[:sc]) if params[:sc].present?
       items = items.where('price < ?', params[:price]) if params[:price].present?
       items = items.order(params[:sort]) if params[:sort]
       items = items.page(page).per(per)
       return items
    end

    def sub_category
        return nil unless params[:sc]
        @category.item_sub_categories.find(params[:sc])    
    end

    def page
        params[:page] || 0
    end

    def per 
        params[:per] || 20
    end
    




end
