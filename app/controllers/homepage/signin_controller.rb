class Homepage::SigninController < Homepage::HomepageController
    layout 'homepage_full' 

    def show
        return redirect_to inventory_landing_path if logged_in?
    end

    def signin
        user = User.joins(:email).find_by(emails: { address: params[:email] })
        if user && user.try(:authenticate, params[:password])
            session[:user_id] = user.id
            redirect_to inventory_landing_path
        else
            @error = "Email address or password not valid."
            render 'show' 
        end
    end

    def signout
        session[:user_id] = nil
        redirect_to homepage_signin_path
    end

    def reset_form
    end

    def send_reset_email
        @user = User.joins(:email).find_by(emails: { address: params[:email]})
        if @user 
            @user.update(password_recovery_token: SecureRandom.hex(10))
            UserMailer.reset_password(@user).deliver
            render 'reset_email_sent'
        else
            @error = "Email address not found."
            render 'reset_form' 
        end
    end

    def reset_password
        @user = User.find_by_password_recovery_token(params[:token])
        unless @user 
            @error = "The reset link you used was invalid or has expired. Enter your email address below to reset your password."
            render 'reset_form'
        end
    end

    def update_password
        @user = User.find_by_password_recovery_token(params[:token])
        return render 'reset_form' unless @user
        @user.password = params[:password]
        @user.password_confirmation = params[:password_confirmation]
        @user.password_recovery_token = nil
        if @user.save
            session[:user_id] = @user.id
            redirect_to inventory_landing_path
        else
            render 'reset_password'
        end
    end

end
