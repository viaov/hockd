class Homepage::SignupController < Homepage::HomepageController
    layout 'homepage_full'
    if Rails.env.production? 
        force_ssl
    end

    def signup
        build_instance
        build_user
    end

    def do_signup
        @instance = Instance.new(signup_params)
        @user = User.new(user_params)
        @user.instance = @instance
        @instance.valid?
        @user.valid?
        if @instance.valid? && @user.valid?
            @instance.save
            @user.save
            session[:user_id] = @user.id
            redirect_to homepage_billing_path
        else
            render 'signup'
        end
    end

    def billing

    end

    def do_billing
        current_instance.customer.cards.create(:card => params[:stripe_token])
        current_instance.customer.subscriptions.create(:plan => 'basic')
        complete_signup
        redirect_to inventory_landing_path
    end

    def complete
    end


    private 

    def complete_signup
        UserMailer.welcome(current_user).deliver
        sns_message = " Company: #{current_instance.company.company_name}\nInstance name: #{current_instance.instance_name}"
        AWS::SNS.new.topics['arn:aws:sns:us-east-1:357993104421:hockd_system'].publish sns_message, subject: 'New Signup'
        current_instance.landing_triggers.create(view_name: 'getting_started')
    end

    def build_instance
        @instance ||= Instance.new
        @instance.build_company unless @instance.company
        @instance.company.build_location unless @instance.company.location
        @instance.company.build_phone_number unless @instance.company.phone_number
    end

    def build_user 
        @user ||= User.new
        @user.build_phone_number unless @user.phone_number
        @user.build_location unless @user.location
        @user.build_email unless @user.email
    end

    def signup_params 
        permitted =  params.require(:instance).permit(
            :instance_name, 
            :company_attributes => [
                :company_name,
                location_attributes: [:street_address, :city, :state, :postal_code],
                phone_number_attributes: [:number]
        ]
        )
    end
    def user_params
        params.require(:user).permit(
            :first_name, 
            :last_name, 
            :password, :password_confirmation,
            email_attributes: [ :address ],
            phone_number_attributes: [ :number ],
            location_attributes: [:street_address, :city, :state, :postal_code],
        )
    end


end
