class Homepage::HomepageController < ApplicationController 
    layout 'homepage'
    force_ssl if Rails.env.production?

    def current_instance 
       @current_instance ||= current_user.try(:instance)
    end
    helper_method :current_instance

    def current_user
        @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end
    helper_method :current_user


    def logged_in? 
        session[:user_id].present?
    end
    helper_method :logged_in?

end
