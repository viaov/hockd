animate_browsers = ->
    setTimeout do_animation, 1000
do_animation = ->
    $('.browsers').transition {height: 392}, 900, 'cubic-bezier(0,1,0.5,1.3)', ->
        $('.browsers').css('padding-top', '5em')
        $('.browsers .browser').transition {opacity: 1}, 400, 'cubic-bezier(0,1,0.5,1.3)', ->
        $('.browsers .browser').eq(1).transition {opacity: 1}, 400, 'cubic-bezier(0,1,0.5,1.3)', ->
            $('.browsers .browser').eq(0).transition {opacity: 1}, 400, 'cubic-bezier(0,1,0.5,1.3)', ->
                $('.browsers .browser').eq(2).transition {opacity: 1}, 400, 'cubic-bezier(0,1,0.5,1.3)', ->


