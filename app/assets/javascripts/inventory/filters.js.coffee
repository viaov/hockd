$.fn.search_button = ->
    return $(@).each ->
        $(@).on 'click', (e) ->
            e.preventDefault()
            backdrop = $('<div class="search-backdrop"></div>')
            #$(document.body).append(backdrop)
            search_box = $('.search-box')
            search_box.find('a.button').on 'click', ->
                $('.search-box').transition {opacity: 0, scale: 0.8}, 500, 'cubic-bezier(0,1,0.5,1.3)', ->
                    $('.search-box').hide()
            backdrop.on 'click', ->
                $(@).remove()
                search_box.transition {opacity: 0, scale: 0.8}, 500, 'ease', ->
                    search_box.hide()
            search_box.css 'left', $(@).position().left
            search_box.css 'top', $(@).position().top + $(@).height() + 10
            search_box.show()
            search_box.css {scale: 0.8}
            search_box.transition {opacity: 1, scale: 1}, 500, 'cubic-bezier(0,1,0.5,1.3)', ->
