setup_sub_category_select = ->
    $('#item_item_category_id').on 'change', ->
        return if $(@).val() == undefined || $(@).val() == ''
        $.get "/inventory/item_categories/#{$(@).val()}/item_sub_categories.json", (data) ->
            items = []
            for c in data
                items.push {id: c.id, text: c.name}
            $('#item_item_sub_category_id').select2
                data: items
                width: 300
    $('#item_item_sub_category_id').select2
        width: 300
        data: []
    $('#item_item_category_id').trigger 'change'


$ setup_sub_category_select
$(document).on 'page:load' , setup_sub_category_select

