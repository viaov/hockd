




setup_lightbox = ->
    $('a[data-lightbox=true]').click (e) ->
        e.preventDefault()
        img_url = $(@).attr('href')
        $("""<div class="lightbox"><div class="lightbox-container"><img src="#{img_url}"/></div></div>""")

