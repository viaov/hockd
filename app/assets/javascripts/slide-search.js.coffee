setup_slide_search = ->
    $('div.slide-search button').click (e)->
        e.preventDefault()
        $('div.slide-search .search-container').transition {width: 150}, 500, 'ease', ->
            $('div.slide-search .search-container input').css
                display: 'inline'
                opacity: 0
            $('div.slide-search .search-container input').transition {opacity: 1}



$ setup_slide_search
$(document).on 'page:load', setup_slide_search


