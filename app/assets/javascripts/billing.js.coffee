setup_billing = ->
    $('.new-card-button').on 'click', (e) ->
        e.preventDefault()
        $('.stripe-error').html(' ')
        Stripe.card.createToken { number: $('#card-number').val(), cvc: $('#card-cvc').val(), exp_month: $('#card-exp-month').val(), exp_year: $('#card-exp-year').val() }, (status, response) ->
            if response.error
                console.log response
                $('.stripe-error').html response.error.message
            if response.card
                $('#stripe_token').val(response.id)
                $('#card-form').submit()


$(document).on 'page:load', setup_billing
$ setup_billing





