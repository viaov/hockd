setup_components = ->
    $('select').each (index, el) ->
        width = $(el).data('width') || '100%'
        $(el).select2
            width: width
            allowClear: true

    $('a[data-modal="true"]').modal()
    $('a[data-searchbox="true"]').search_button()

$ ->
    setup_components()
$(document).on 'page:load', ->
    setup_components()
