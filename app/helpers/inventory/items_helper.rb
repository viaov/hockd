module Inventory::ItemsHelper


    def detail_item(label_text, value_text) 
        content_tag :div do
            concat content_tag :span, label_text, class: 'label'
            concat content_tag :span, value_text, class: 'value'
        end
    end

end
