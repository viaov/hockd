module Inventory::InventoryHelper 
    def side_nav_link(title, url, opts={}) 
        active_class = nil
        if opts[:controller] 
            if opts[:controller].is_a? Array
                active_class = "active" if opts[:controller].include? params[:controller].classify
            end
            if opts[:controller].is_a? String
                active_class = "active" if opts[:controller] == params[:controller].classify
            end
        end
        content_tag :li, class: active_class do
            link_to title, url
        end
    end

    def boolean_label(value) 
        cls = "boolean-label"
        cls += " true" if value == true
        cls += " false" if value == false
        value_string = (value==true ? 'yes' : 'no')
        content_tag :div, value_string, class: cls
    end

end
