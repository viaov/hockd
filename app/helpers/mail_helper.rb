module MailHelper
    def button(text, link=nil, opts: {})
       link ||= text 
       opts[:style] ||= ''
       link_to text, link, style: "font-family: sans-serif; margin-top: 10px; margin-bottom: 20px;display: block; border: 1px solid #076fe4; border-radius: 3px; box-shadow: inset 0 1px 0 0 #8ebcf1; color: white; display: inline-block; font-size: inherit; font-weight: bold; background-color: #4294f0; background-image: -webkit-linear-gradient(#4294f0, #0776f3); background-image: linear-gradient(#4294f0, #0776f3); padding: 7px 18px; text-decoration: none; text-shadow: 0 1px 0 #0065d6; background-clip: padding-box;#{opts[:style]}"
    end
    def mail_link text, link=nil, style=nil
        inline_style = "colro: 4294f0";
        inline_style = "color: #333;" if style == "muted"
        link ||= text
        link_to text, link, style: inline_style
    end

end
