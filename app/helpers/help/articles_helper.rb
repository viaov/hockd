module Help::ArticlesHelper

    def help_nav_link(title, article)
        content_tag :li, class: (params[:article] == article ? 'active' :nil) do 
            link_to title, help_article_path(article)
        end


    end

end
