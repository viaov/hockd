module Browser::ItemBrowserHelper


    def featured_item(item)
        render partial: 'browser/featured_item', object: item 
    end


    def item_display_style
        params[:d] || 'list'
    end

    def display_style_link (icon, display_value) 
        active_class = (item_display_style == display_value ? 'active' : nil)
        link_to icon(icon), url_for(params.merge(d: display_value)), class: active_class
    end

    def filter_link(param, text, value=nil) 
        merge_params = {}
        merge_params[param] = value
        if value 
            content_tag :li, class: (params[param] == value.to_s ? 'active' : nil) do 
                link_to text, url_for(params.merge(merge_params))
            end
        else
            active_class = 'active' if params[param].nil?
            content_tag :li, class: active_class do 
                link_to 'All', url_for(params.merge(merge_params))
            end 
        end
    end

end
