module Browser::BrowserHelper
    def browser_page_title 
        "#{current_instance.company.company_name} - #{number_to_phone current_instance.company.phone_number.number} - #{current_instance.company.location.address}"
    end
end
