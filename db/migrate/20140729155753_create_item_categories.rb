class CreateItemCategories < ActiveRecord::Migration
    def change
        create_table :item_categories do |t|
            t.belongs_to :instance
            t.belongs_to :item_category
            t.string :name

            t.timestamps
        end
    end
end
