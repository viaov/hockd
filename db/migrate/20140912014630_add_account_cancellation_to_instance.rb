class AddAccountCancellationToInstance < ActiveRecord::Migration
    def change
        add_column :instances, :cancelled, :boolean, default: false
        add_column :instances, :cancellation_reason, :string
    end
end
