class CreateInstances < ActiveRecord::Migration
  def change
    create_table :instances do |t|
      t.string :instance_name
      t.belongs_to :company
      t.belongs_to :apperance
      t.belongs_to :payment_method
      t.string :identifier
      t.string :stripe_token
      t.string :default_card_token
      t.boolean :billing_valid, default: false
      t.timestamps
    end
  end
end
