class CreateItemPhotos < ActiveRecord::Migration
    def change
        create_table :item_photos do |t|
            t.belongs_to :item
            t.boolean :primary, default: false
            t.timestamps
        end
        add_attachment :item_photos, :image
    end
end
