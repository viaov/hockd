class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :street_address
      t.string :city
      t.string :state
      t.string :postal_code
      t.decimal :latitude
      t.decimal :longitude
      t.timestamps
    end
  end
end
