class CreateLandingTriggers < ActiveRecord::Migration
    def change
        create_table :landing_triggers do |t|
            t.references :landing_triggerable, polymorphic: true 
            t.string :view_name
            t.boolean :presented, default: false
            t.datetime :presented_at
            t.boolean :auto_remove, default: true
            t.boolean :forceful, default: false
            t.timestamps
        end
    end
end
