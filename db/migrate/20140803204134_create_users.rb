class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.belongs_to :email
      t.belongs_to :phone_number
      t.belongs_to :location
      t.belongs_to :instance
      t.string :first_name
      t.string :last_name
      t.string :password_digest
      t.timestamps
    end
  end
end
