class CreateItems < ActiveRecord::Migration
    def change
        create_table :items do |t|
            t.belongs_to :item_category
            t.belongs_to :item_sub_category
            t.belongs_to :instance
            t.string :identifier
            t.string :title
            t.text :description
            t.decimal :price, scale: 2, precision: 6
            t.string :condition, default: 'Good'
            t.string :isbn
            t.boolean :featured
            t.datetime :last_viewed_at
            t.integer :view_count,default: 0
            t.timestamps
        end
    end
end
