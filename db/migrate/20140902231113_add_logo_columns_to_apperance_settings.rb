class AddLogoColumnsToApperanceSettings < ActiveRecord::Migration
  def change
    add_attachment :apperance_settings, :logo_image
    add_column :apperance_settings, :full_size_logo, :boolean
  end
end
