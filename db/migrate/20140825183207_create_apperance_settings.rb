class CreateApperanceSettings < ActiveRecord::Migration
    def change
        create_table :apperance_settings do |t|
            t.belongs_to :instance
            t.string :theme_name, default: 'hydrogen'
            t.string :scheme_name, default: 'black'
            t.timestamps
        end
    end
end
