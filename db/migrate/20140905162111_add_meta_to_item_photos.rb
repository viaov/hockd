class AddMetaToItemPhotos < ActiveRecord::Migration
  def change
      add_column :item_photos, :image_meta, :text
  end
end
