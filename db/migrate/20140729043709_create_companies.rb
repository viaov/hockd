class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :company_name
      t.belongs_to :instance
      t.belongs_to :phone_number
      t.belongs_to :location
      t.timestamps
    end
  end
end
