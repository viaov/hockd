# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140912014630) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "apperance_settings", force: true do |t|
    t.integer  "instance_id"
    t.string   "theme_name",              default: "hydrogen"
    t.string   "scheme_name",             default: "black"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "logo_image_file_name"
    t.string   "logo_image_content_type"
    t.integer  "logo_image_file_size"
    t.datetime "logo_image_updated_at"
    t.boolean  "full_size_logo"
    t.text     "logo_image_meta"
  end

  create_table "companies", force: true do |t|
    t.string   "company_name"
    t.integer  "instance_id"
    t.integer  "phone_number_id"
    t.integer  "location_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "emails", force: true do |t|
    t.string   "address"
    t.boolean  "contactable"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "instances", force: true do |t|
    t.string   "instance_name"
    t.integer  "company_id"
    t.integer  "apperance_id"
    t.integer  "payment_method_id"
    t.string   "identifier"
    t.string   "stripe_token"
    t.string   "default_card_token"
    t.boolean  "billing_valid",       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "cancelled",           default: false
    t.string   "cancellation_reason"
  end

  create_table "item_categories", force: true do |t|
    t.integer  "instance_id"
    t.integer  "item_category_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "item_photos", force: true do |t|
    t.integer  "item_id"
    t.boolean  "primary",            default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.text     "image_meta"
  end

  create_table "item_sub_categories", force: true do |t|
    t.integer  "item_category_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "items", force: true do |t|
    t.integer  "item_category_id"
    t.integer  "item_sub_category_id"
    t.integer  "instance_id"
    t.string   "identifier"
    t.string   "title"
    t.text     "description"
    t.decimal  "price",                precision: 6, scale: 2
    t.string   "condition",                                    default: "Good"
    t.string   "isbn"
    t.boolean  "featured"
    t.datetime "last_viewed_at"
    t.integer  "view_count",                                   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "properties"
  end

  create_table "landing_triggers", force: true do |t|
    t.integer  "landing_triggerable_id"
    t.string   "landing_triggerable_type"
    t.string   "view_name"
    t.boolean  "presented",                default: false
    t.datetime "presented_at"
    t.boolean  "auto_remove",              default: true
    t.boolean  "forceful",                 default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: true do |t|
    t.string   "street_address"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "phone_numbers", force: true do |t|
    t.string   "number"
    t.string   "number_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.integer  "email_id"
    t.integer  "phone_number_id"
    t.integer  "location_id"
    t.integer  "instance_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_recovery_token"
  end

end
